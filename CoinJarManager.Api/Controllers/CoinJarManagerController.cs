﻿using System;
using AutoMapper;
using CoinJarManager.Api.Core;
using Microsoft.AspNetCore.Mvc;
using CoinJarManager.Api.Models;
using System.Globalization;
using CoinJarManager.Api.Utils;
using System.Threading.Tasks;

namespace CoinJarManager.Api.Controllers
{
	[Route("api/coinjar")]
	[ApiController]
	public class CoinJarManagerController : ControllerBase
	{
		private readonly IMapper _mapper;
		private readonly ICoinJar _coinJar;
		private const string UNITS = "C";

		public CoinJarManagerController(ICoinJar coinJar, IMapper mapper)
		{
			_mapper = mapper;
			_coinJar = coinJar;
		}

		[HttpPost("add-coin")]
		public async Task<IActionResult> AddCoin([FromBody]CoinJarRequest request)
		{
			try
			{
				if(request.Amount == 0 || request.Volume == 0)
				{
					return BadRequest("(-^-)");
				}

				var coin = _mapper.Map<Coin>(request);
				await _coinJar.AddCoin(coin);
				return Ok();
			}
			catch (Exception e)
			{				
				return StatusCode(500, ErrorsPool.ErrorAddingCoin(e));
			}
		}

		[HttpGet("get-total-amount")]
		public async Task<IActionResult> GetTotalAmount()
		{
			var totalAmount = await _coinJar.GetTotalAmount();
			return Ok(totalAmount.ToString(UNITS, CultureInfo.CreateSpecificCulture(CurrencyHelper.US)));
		}

		[HttpPut("reset-total")]
		public async Task<IActionResult> Reset()
		{
			try
			{
				await _coinJar.Reset();
				return Ok();
			}
			catch (Exception e)
			{
				return StatusCode(500, ErrorsPool.ErrorResetting(e));
			}
		}
	}
}
