﻿using System;
using System.Text.Json;
using System.Threading.Tasks;
using CoinJarManager.Api.Data;
using CoinJarManager.Api.Models;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;

namespace CoinJarManager.Api.Core
{
	public class CoinJar : ICoinJar
	{
		private const int MAX_VOLUME = 42;		
		private readonly ILogger<CoinJar> _logger;
		private readonly ICoinRepository _coinRepository;

		public CoinJar(ICoinRepository coinRepository, ILogger<CoinJar> logger)
		{
			_logger = logger;
			_coinRepository = coinRepository;
		}

		public async Task AddCoin(ICoin coin)
		{
			try
			{
				ICoin coinage = await _coinRepository.GetCoinDetails();
				if (coinage != null && coinage.Volume < MAX_VOLUME)
				{
					coinage.Volume += coin.Volume;
					coinage.Amount += (coin.Amount * coin.Volume);
					await _coinRepository.InsertCoin(coinage);
				}			
			}
			catch (Exception e)
			{
				_logger.LogError("Problem: ", e);
				throw;
			}
		}

		public async Task<decimal> GetTotalAmount()
		{
			ICoin coinage = await _coinRepository.GetCoinDetails();
			if (coinage != null)
			{
				return coinage.Amount;
			}

			return 0;
		}

		public async Task Reset()
		{
			await _coinRepository.ResetValue();
		}		
	}
}
