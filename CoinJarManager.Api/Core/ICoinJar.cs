﻿using CoinJarManager.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoinJarManager.Api.Core
{
	public interface ICoinJar
	{
		Task AddCoin(ICoin coin);
		Task<decimal> GetTotalAmount();
		Task Reset();
	}
}
