﻿using CoinJarManager.Api.Models;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace CoinJarManager.Api.Data
{
	public class CoinRepository : ICoinRepository
	{
		private const string DATABASE_KEY = "coinage";
		private readonly IMemoryCache _databaseContext;
		private readonly ILogger<CoinRepository> _logger;

		public CoinRepository(IMemoryCache databaseContext, ILogger<CoinRepository> logger)
		{
			_logger = logger;
			_databaseContext = databaseContext;
		}

		public async Task<ICoin> GetCoinDetails()
		{
			try
			{
				ICoin coinage = new Coin();
				_databaseContext.TryGetValue(DATABASE_KEY, out string currentCoins);
				if (!string.IsNullOrEmpty(currentCoins))
				{
					coinage = JsonSerializer.Deserialize<Coin>(currentCoins);
				}
				return await Task.FromResult(coinage);
			}
			catch (Exception e)
			{
				_logger.LogError("Problem: ", e);
				throw;
			}
		}

		public async Task InsertCoin(ICoin coin)
		{
			try
			{				
				_databaseContext.Set(DATABASE_KEY, JsonSerializer.Serialize(coin));
				await Task.CompletedTask;
			}
			catch (Exception e)
			{
				_logger.LogError("Problem: ", e);
				throw;
			}
		}

		public async Task ResetValue()
		{
			try
			{
				_databaseContext.Remove(DATABASE_KEY);
				await Task.CompletedTask;
			}
			catch (Exception e)
			{
				_logger.LogError("Problem: ", e);
				throw;
			}
		}
	}
}
