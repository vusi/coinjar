﻿using CoinJarManager.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoinJarManager.Api.Data
{
	public interface ICoinRepository
	{
		Task<ICoin> GetCoinDetails();
		Task InsertCoin(ICoin coin);
		Task ResetValue();
	}
}
