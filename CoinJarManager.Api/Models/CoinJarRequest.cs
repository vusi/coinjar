﻿using System.ComponentModel.DataAnnotations;

namespace CoinJarManager.Api.Models
{
	public class CoinJarRequest
	{
		[Required]
		public decimal Amount { get; set; }

		[Required]
		public decimal Volume { get; set; }
	}
}
