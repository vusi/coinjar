﻿
namespace CoinJarManager.Api.Models
{
 	public interface ICoin
	{
		decimal Amount { get; set; }
		decimal Volume { get; set; }
	}
}
