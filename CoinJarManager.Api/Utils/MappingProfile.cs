﻿using AutoMapper;
using CoinJarManager.Api.Models;

namespace CoinJarManager.Api.Utils
{
	public class MappingProfile: Profile
	{
		public MappingProfile()
		{
			CreateMap<CoinJarRequest, Coin>();
		}
	}
}
