using Moq;
using Xunit;
using CoinJarManager.Api.Core;
using CoinJarManager.Api.Data;
using CoinJarManager.Api.Models;
using Microsoft.Extensions.Logging;

namespace CoinJarManager.Tests
{	
	public class CoinJarTests
	{
		private ICoinJar serviceInTest;
		private readonly Mock<ILogger<CoinJar>> _loggerMock;
		private readonly Mock<ICoinRepository> _coinRepositoryMock;

		public CoinJarTests()
		{
			_loggerMock = new Mock<ILogger<CoinJar>>();
			_coinRepositoryMock = new Mock<ICoinRepository>();
		}

		[Fact]
		public void For_AddCoin_Given_Coin_Should_Insert_Into_Database()
		{
			// Arrange
			var coin = new Coin { Volume = 1, Amount = 10 };
			_coinRepositoryMock.Setup(x => x.GetCoinDetails()).ReturnsAsync(new Coin());

			//Act
			serviceInTest = new CoinJar(_coinRepositoryMock.Object, _loggerMock.Object);
			var result = serviceInTest.AddCoin(coin);


			//Assert
			_coinRepositoryMock.Verify(x => x.InsertCoin(It.IsAny<Coin>()), Times.Once);
		}

		[Fact]
		public async void For_GetCoinsTotalValue_Should_Return_Numeric_Value()
		{
			// Arrange
			var coin = new Coin { Volume = 1, Amount = 10 };
			_coinRepositoryMock.Setup(x => x.GetCoinDetails()).ReturnsAsync(coin);

			//Act
			serviceInTest = new CoinJar(_coinRepositoryMock.Object, _loggerMock.Object);
			var result = await serviceInTest.GetTotalAmount();


			//Assert
			Assert.Equal(10, result);
		}
	}
}
