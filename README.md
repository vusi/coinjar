## Coin Jar API

The purpose of this api is to manage the US coinage reserves. 

## Getting Started

1  Clone & Run swagger UI

```
    clone https://gitlab.com/vusi/coinjar.git
    cd  CoinJarManager.Api
    dotnet build
    dotnet run       
```

2  Navigate to [https://localhost:5001/swagger](https://localhost:5001/swagger)


3  Run Unit Tests

```    
    cd ..
    cd  CoinJarManager.Tests    
    dotnet test 
```

## License

MIT License

## Contributing

Vusumuzi Magayiyana
